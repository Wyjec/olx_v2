import sys
import time
import re
import urllib2
import argparse
import textwrap

try:
  from sendmail import SendMail
except ImportError:
  raise ImportError('Sendmail module missing')

class colors:
    RED = '\033[91m'
    GREEN = '\033[92m'
    END = '\033[0m'  
  
urlsSaved=[]

ROOTOLXURL="www.olx.pl/oferta"
#I feel embarrassed beacuse of this Regex
OLXPATTERN='href="https://www.olx.pl/oferta(.*?)".*?"price">.*?<strong>(.*?)z.*?marginbott5 x-normal">(.*?)<'
ALLEGROPATTERN='lista ofert</h2></div>.*?</section>'

  
###############################################################################

#HTML Content
def Content(url):
  try:
    html = urllib2.urlopen(url)
    html = html.read().replace('\n', ' ').replace("\t", "")
  except urllib2.HTTPError, e:
    import requests
    try:
      request = requests.get(url)
      html = request.content.replace('\n', ' ').replace("\t", "")
    except:
      return ""
  return html

#Olx Searcher
def Olx(item, city, minPrice, maxPrice):
  tempUrl=[]
  item=item.replace(" ", "-")
  if city:
    url="https://www.olx.pl/%s/q-%s" %(city, item)
  else:
    url="https://www.olx.pl/oferty/q-"+item
	
  html = Content(url)
  if not html:
    print "%sCan't retrieve any OLX content.%s" % (colors.RED, colors.END)
    return

  for m in re.finditer(OLXPATTERN, html):
    if re.search( '(dzisiaj|wczoraj)', m.group(3)) is not None:
      offer=m.group(1)
      if re.match("^[0-9]", m.group(2)):
	price=float(m.group(2).replace(" ", "").replace(",","."))
  	if price >= minPrice and price <= maxPrice:
          if offer not in urlsSaved:
            urlsSaved.append(offer)
            offer=ROOTOLXURL+offer
            tempUrl.append(offer)
  if tempUrl:
    SendMail(tempUrl, item, "Olx")
            
  

#Allegro filter -- New, Used, Auction or Buy Now Items
def AllegroPadding(data):
   padding=""
   for singleData in data:
     padding+={
     'A': "offerTypeAuction=2",
     'U': "buyUsed=1",
     'N': "buyNew=1",
     'B': "offerTypeBuyNow=1",
     }[singleData]
     padding+="&"
     
   return padding[:-1]


#Allegro Searcher
def Allegro(item, city, minPrice, maxPrice, data):
  linkRegex='href="http://(.*?)"'
  item = item.replace(" ","%20")
  tempUrl=[]
  url = "https://allegro.pl/listing?string=%s&order=n&price_from=%s&price_to=%s&city=%s&"  % (item, minPrice, maxPrice, city)
  url = url + AllegroPadding(data)
  html = Content(url)
  if not html:
    print "%sCan't retrieve any Allegro content.%s" % (colors.RED, colors.END)
    return
  
  for m in re.findall(ALLEGROPATTERN, html):
    for x in re.finditer(linkRegex, m):
      offer=x.group(1)
      if offer not in urlsSaved:
        tempUrl.append( offer)
        urlsSaved.append(offer)
  
  if tempUrl:
    SendMail(tempUrl[:10], item, "Allegro")

#Gaining data from user
def GetData():
  parser = argparse.ArgumentParser(prog="olx.py", formatter_class=argparse.RawDescriptionHelpFormatter,
  	                           description=textwrap.dedent('''%s
  Example of usage:
  python olx.py --allegro A U -p --item nintendo gamecube
  python olx.py --city gdansk --item nintendo gamecube
  	                           	                       %s''' % (colors.GREEN, colors.END)))
  
  parser.add_argument("--item", help="Searching item", nargs='+', metavar="item")
  parser.add_argument("--allegro", help="Include Allegro. [A]uction [B]uy_now [U]sed [N]ew ", nargs="+", choices=['A','B','U','N'])
  parser.add_argument("--city", help="City restriction. Type city name", metavar="city")
  parser.add_argument("-p", "--price", action="store_true", help="Price restriction")
  args = parser.parse_args()
  if not args.item:
    print "You need to specify an item"
    sys.exit()
    
  item = " ".join(args.item)
  while not re.match("^[a-zA-Z ]+$", item):
    item=str(raw_input("Enter the item without polish characters: "))
    
  city=args.city
  if city:
    while (not re.match("^[a-zA-Z]+$", city)):
      city=str(raw_input("Enter the city without polish characters: "))
  else:
    city=""

  if args.price:
    try:
      minPrice = float(raw_input("Min price to search: ") or 0)
    except:
      print "incorrect input. Expected a number. Set to 0"
      minPrice=0
      
    try:
      maxPrice = float(raw_input("Max price to search: ") or sys.maxint)
    except:
      print "incorrect input. Expected a number. Set to max"
      maxPrice=sys.maxint
  else:
    minPrice = 0
    maxPrice = sys.maxint
  
  return item, city, minPrice, maxPrice, args.allegro

def Main():
  if sys.version_info[0] > 2:
    raise "Only for Python 2.7 version usage"
  
  item, city, minPrice, maxPrice, isAllegro = GetData()
  
  #Search begins
  while True:
    Olx(item, city, minPrice, maxPrice)
    if isAllegro:
      Allegro(item, city, minPrice, maxPrice, isAllegro)
    time.sleep(120)
    
  
if __name__ == "__main__":
  Main()

	
